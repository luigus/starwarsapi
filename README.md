# UAI - StartWars

### Techs

* [Python 3.7] - Linguagem principal do projeto 
* [Flask] - Micro-framework web em Python
* [Pipenv] - Instalador de pacotes Python 
* [SQLite3] - Banco de dados para prototipagem
* [Ubuntu 18.04] - Sistema operacional Linux

Todo o código está disponível em um repositótio privado no Bitbucket.


### Clone project

```sh
$ git clone https://luigus@bitbucket.org/luigus/starwarsapi.git 
```

### Preparando o ambiente virtual
```sh
$ cd starwarsapi/web
$ mkdir .venv
$ pipenv --python 3.7
$ pipenv shell
$ pipenv install
```

### Criação do banco de dados
É necessário criar o banco de dados local planets.db antes de rodar o projeto pela primeira vez.

```sh 
$ cd starwarsapi/web/db
$ python3 create_database.py
```

### Criação da Docker Image
```sh
$ sudo docker build -t flask-image -f Dockerfile .
```
### Rodar Docker Container
```sh
$ sudo docker run -it -p 5000:5000 flask-image
```

### Rodar o projeto localmente (no caso de não utilizar o Docker)
```sh
$ cd starwarsapi/web/
$ python app.py
```

### API
Lista de chamadas da API:

| API 				   | METHOD 	| ENDPOINTS 				    |
| ------ 			   | ------ 	|------ 		                |
| Lista de planetas    | GET 		| /api/v1/planets   	        | 
| Dados de um planeta  | GET 		| /api/v1/planets/<id_planeta>	| 


### Ordenação
É possível fazer chamadas na api ordenando os resultados por name ou population conforme foi requerido.

 - GET /api/v1/planets/?sort=name asc

### Paginação
A chamada da API sempre vai mostrar no máximo 10 resultados por página, é possível ver os resultados de outras páginas.

 - GET /api/v1/planets/?page=3
   
### Busca
É possível fazer busca por planetas por nome ou por clima

 - GET /api/v1/planets/?search=Tatooi
 - GET /api/v1/planets/?climate=arid

### Observação
Todos os recursos podem ser chamados de uma só vez

 - GET /api/planets/?climate=arid&search=tatooi&page=2&sort=name asc
