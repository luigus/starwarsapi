import os
from flask import Flask, jsonify, request
from sql_alchemy import database
from models import Planets, Serializer

PROJECT_ROOT = os.path.join(os.path.abspath(os.path.dirname(__file__))+'/db/', "planets.db")

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:////{PROJECT_ROOT}"
app.config["SECRET_KEY"] = b'\xed\xf4\x83\xac\x92\x948\x10\xed\x04r\x94\x90\x058\xec\xf5\x84\x8bV\xfe\xceb\xea'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['MAX_PER_PAGE'] = 10

database.init_app(app)


@app.route('/api/v1/planets/', methods=['GET'])
def index():
    per_page = app.config['MAX_PER_PAGE']
    page = request.args.get('page', 1, type=int)
    sort = request.args.get('sort', '', type=str)
    search = request.args.get('search', '', type=str)
    climate = request.args.get('climate', '', type=str)

    dic_sord = {
        'name asc': Planets.name.asc(),
        'name desc': Planets.name.desc(),
        'population asc': Planets.population.asc(),
        'population desc': Planets.population.desc()
    }

    planets = Planets.query

    if search != '':
        planets = planets.filter(Planets.name.like('%' + search + '%'))

    if climate != '':
        planets = planets.filter(Planets.climate.like('%' + climate + '%'))

    if sort != '':
        if sort in dic_sord:
            planets = planets.order_by(dic_sord[sort]).paginate(page, per_page, False)
    else:
        planets = planets.paginate(page, per_page, False)

    return jsonify(Serializer.serialize_list(planets.items))


@app.route('/api/v1/planets/<id_planet>', methods=['GET'])
def planet(id_planet):
    p = Planets.query.get(id_planet)
    return jsonify(p.serialize())


if __name__ == '__main__':
    app.run(debug=True)
