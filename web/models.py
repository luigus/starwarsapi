from sql_alchemy import database as db
from sqlalchemy.inspection import inspect


class Serializer(object):

    def serialize(self):
        return {c: getattr(self, c) for c in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(_list):
        return [m.serialize() for m in _list]


class Planets(db.Model):
    __tablename__ = "planets"

    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    rotation_period = db.Column(db.String(100), nullable=False)
    orbital_period = db.Column(db.String(100), nullable=False)
    diameter = db.Column(db.String(100), nullable=False)
    climate = db.Column(db.String(100), nullable=False)
    gravity = db.Column(db.String(100), nullable=False)
    terrain = db.Column(db.String(100), nullable=False)
    surface_water = db.Column(db.String(100), nullable=False)
    population = db.Column(db.String(100), nullable=False)
    created = db.Column(db.String(100), nullable=False)
    edited = db.Column(db.String(100), nullable=False)

    def __str__(self):
        return self.name

    def serialize(self):
        return Serializer.serialize(self)
