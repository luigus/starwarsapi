

def test_list_planets(app, client):
    res = client.get('/api/v1/planets/')
    assert res.status_code == 200


def test_list_planets_page(app, client):
    res = client.get('/api/v1/planets/?page=2')
    assert res.status_code == 200


def test_list_planets_page_climate(app, client):
    res = client.get('/api/v1/planets/?page=2&climate=arid')
    assert res.status_code == 200


def test_list_planets_page_search(app, client):
    res = client.get('/api/v1/planets/?page=2&search=Kale')
    assert res.status_code == 200


def test_list_planets_page_search_climate(app, client):
    res = client.get('/api/v1/planets/?page=2&search=Kale&climate=arid')
    assert res.status_code == 200


def test_list_planets_page_search_climate_sort_name_asc(app, client):
    res = client.get('/api/v1/planets/?page=2&search=Kale&climate=arid&sort=name asc')
    assert res.status_code == 200


def test_list_planets_page_search_climate_sort_name_desc(app, client):
    res = client.get('/api/v1/planets/?page=2&search=Kale&climate=arid&sort=name desc')
    assert res.status_code == 200


def test_list_planets_page_search_climate_sort_population_asc(app, client):
    res = client.get('/api/v1/planets/?page=2&search=Kale&climate=arid&sort=population asc')
    assert res.status_code == 200


def test_list_planets_page_search_climate_sort_population_desc(app, client):
    res = client.get('/api/v1/planets/?page=2&search=Kale&climate=arid&sort=population desc')
    assert res.status_code == 200

