import json
import sqlite3
import requests
import asyncio
import os
from os import listdir
from os.path import isfile, join

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))


async def planets_from_swapi(page):

    try:
        url = f"https://swapi.dev/api/planets/?page={page}"
        result = requests.get(url)
        dic = result.json()

        del dic['count']
        del dic['next']
        del dic['previous']

        return dic
    except Exception as e:
        print(e.args)


async def write_json_file(_json, _file):
    _file.write(json.dumps(_json))
    _file.close()


async def create_files():

    for page in range(1, 7):
        _file = open(f"planets_{page}.json", 'w')
        _json = await asyncio.gather(planets_from_swapi(page))
        await write_json_file(_json, _file)


def insert_planets_database():

    files_dir = [f for f in listdir(PROJECT_ROOT) if isfile(join(PROJECT_ROOT, f)) and '.py' not in f and '.sh' not in f]

    # Se por algum motivo a API estiver inacessivel, utilizaremos os arquivos de backup para popular o banco
    if len(files_dir) == 0:
        files_dir = [f for f in listdir(PROJECT_ROOT) if isfile(join(PROJECT_ROOT+'backup/mock/', f))]

    setup_database()
    for f in files_dir:

        # Abre o arquivo
        _file = open(f, 'r')

        # Le os dados
        line = _file.readline()

        # Insere o json num dicionario
        dic_planets = json.loads(line)

        conn = create_connection()
        if conn:

            cursor = conn.cursor()
            planets = dic_planets[0]["results"]
            for planet in planets:

                del planet['films']
                del planet['residents']
                del planet['url']

                fields = str(list(planet.keys()))[1:-1]
                values = str(list(planet.values()))[1:-1]
                query = f'INSERT INTO "planets" ({fields}) VALUES ({values});'

                cursor.execute(query)
                conn.commit()

            _file.close()
            os.remove(f)
            cursor.close()


# Create a database connection to the SQLite database
def create_connection():

    conn = None
    database = os.path.join(PROJECT_ROOT, 'planets.db')
    try:
        conn = sqlite3.connect(database)
        return conn
    except sqlite3.Error as e:
        print(e)

    return conn


# Create a table planets
def create_table_planets(conn):

    try:
        create_table = """ CREATE TABLE "planets"(
                            "id" Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
                            "name" Text NOT NULL,
                            "rotation_period" Text NOT NULL,
                            "orbital_period" Text NOT NULL,
                            "diameter" Text NOT NULL,
                            "climate" Text NOT NULL,
                            "gravity" Text NOT NULL,
                            "terrain" Text NOT NULL,
                            "surface_water" Text NOT NULL,
                            "population" Text NOT NULL,
                            "created" DateTime NOT NULL,
                            "edited" DateTime NOT NULL,
                            CONSTRAINT "unique_id" UNIQUE ( "id" ) );"""

        c = conn.cursor()
        c.execute(create_table)

    except sqlite3.Error as e:
        print(e)


def setup_database():
    try:
        conn = create_connection()
        if conn is not None:
            create_table_planets(conn)
    except Exception as e:
        print(e.args)


if __name__ == "__main__":

    # Limpa a pasta antes de iniciar
    os.popen('rm *.json')
    os.popen('rm planets.db')

    # Cria os arquivos de json baseados da api swapi
    asyncio.run(create_files())

    # Cria o banco e insere os dados
    insert_planets_database()

