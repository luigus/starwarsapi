CREATE TABLE "planets"(
    "id" Integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" Text NOT NULL,
    "rotation_period" Text NOT NULL,
    "orbital_period" Text NOT NULL,
    "diameter" Text NOT NULL,
    "climate" Text NOT NULL,
    "gravity" Text NOT NULL,
    "terrain" Text NOT NULL,
    "surface_water" Text NOT NULL,
    "population" Text NOT NULL,
    "created" DateTime NOT NULL,
    "edited" DateTime NOT NULL,
CONSTRAINT "unique_id" UNIQUE ( "id" ) );